// Copyright Epic Games, Inc. All Rights Reserved.

#include "AGPNetworkGame.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, AGPNetworkGame, "AGPNetworkGame" );
 