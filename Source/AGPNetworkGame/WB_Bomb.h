// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "WeaponBase.h"
#include "WB_Bomb.generated.h"

/**
 *
 */
UCLASS()
class AGPNETWORKGAME_API AWB_Bomb : public AWeaponBase
{
	GENERATED_BODY()

public:
	AWB_Bomb();
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void useWeapon(APawn* player) override; //MUST define virtual func!
	UFUNCTION()
		void useWeaponAfterDelay();
	UFUNCTION()
		void destroyWeapon();
	UFUNCTION()
		void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);
	UFUNCTION()
		void OnBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UPROPERTY(EditAnywhere, Category = BombDetails)
		float impulseVal;
	UPROPERTY(EditAnywhere, Category = BombDetails)
		float timeforBombToExplode;
	UPROPERTY(EditAnywhere, Category = BombDetails)
		float damageAmt;
	UPROPERTY(EditAnywhere, Category = BombDetails)
		float blastRadius;
	UPROPERTY(EditAnywhere, Category = HitEffects)
		UParticleSystem* hitParticle;
	UPROPERTY(EditDefaultsOnly, Category = HitEffects)
		USoundBase* bombSound;
	bool onFloor;

};
