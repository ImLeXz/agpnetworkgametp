// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"
#include "Pickup.generated.h"

UCLASS()
class AGPNETWORKGAME_API APickup : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APickup();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void HandlePickup(class AAGPNetworkGameCharacter* chr);
	//-----
	
	class APickupSpawnVolume* volumeSpawnedAt;
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//Handle Pickup
	UFUNCTION(NetMulticast, Reliable)
		virtual void MC_HandlePickup(class AAGPNetworkGameCharacter* chr);
	virtual void MC_HandlePickup_Implementation(class AAGPNetworkGameCharacter* chr);
	
	//Volume Stuff
	UFUNCTION(NetMulticast, Reliable)
		void MC_SetSpawnedAtVolume(class APickupSpawnVolume* volume);
	void MC_SetSpawnedAtVolume_Implementation(class APickupSpawnVolume* volume);

	UFUNCTION(Server, Reliable)
		void ServerSetSpawnedAtVolume(class APickupSpawnVolume* volume);
	void ServerSetSpawnedAtVolume_Implementation(class APickupSpawnVolume* volume);
	void SetSpawnedAtVolume(class APickupSpawnVolume* volume);
	//

	
	
	UPROPERTY(EditAnywhere)
		FString name;

	UPROPERTY(EditAnywhere)
		UBoxComponent* collisionComponent;

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* staticMeshComp;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
		class USoundBase* pickupSound;
	

	UFUNCTION()
		void OnBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
			int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

};
