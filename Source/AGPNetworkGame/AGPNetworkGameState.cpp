// Fill out your copyright notice in the Description page of Project Settings.


#include "AGPNetworkGameState.h"
#include "Net/UnrealNetwork.h" //for replication

AAGPNetworkGameState::AAGPNetworkGameState() {
	gameTime = 0.0f;
}

void AAGPNetworkGameState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const {
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	//Update replicated properties
	DOREPLIFETIME(AAGPNetworkGameState, gameTime);
}
