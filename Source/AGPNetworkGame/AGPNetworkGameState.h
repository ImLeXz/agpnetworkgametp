// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "AGPNetworkGameState.generated.h"

/**
 * 
 */
UCLASS()
class AGPNETWORKGAME_API AAGPNetworkGameState : public AGameStateBase
{
	GENERATED_BODY()
public:
	AAGPNetworkGameState();
	
	/** Marks the properties we wish to replicate */
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	
	UPROPERTY(VisibleAnywhere, Replicated, BlueprintReadWrite, Category = Timer)
		float gameTime;

};
