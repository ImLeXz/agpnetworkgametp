// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Pickup.h"
#include "Health_Pickup.generated.h"

/**
 * 
 */
UCLASS()
class AGPNETWORKGAME_API AHealth_Pickup : public APickup
{
	GENERATED_BODY()
protected:
	virtual void HandlePickup(class AAGPNetworkGameCharacter* chr) override;

	
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = HealthPickup)
		float healthIncrease;
};
