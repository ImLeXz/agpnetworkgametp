// Fill out your copyright notice in the Description page of Project Settings.


#include "SpeedBoost_Pickup.h"
#include "AGPNetworkGameCharacter.h"

void ASpeedBoost_Pickup::HandlePickup(AAGPNetworkGameCharacter* Chr)
{
	if(Chr != nullptr)
	{
		Chr->IncreaseSpeed(speedMultiplier);
		FTimerHandle resetSpeedTimer;
		GetWorldTimerManager().SetTimer(resetSpeedTimer, Chr, &AAGPNetworkGameCharacter::ResetSpeed, speedResetDelay, false);
		Destroy();
	}
}
