// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Pickup.h"
#include "PickupSpawnVolume.h"
#include "GameFramework/GameModeBase.h"
#include "AGPNetworkGameGameMode.generated.h"

UCLASS(minimalapi)
class AAGPNetworkGameGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AAGPNetworkGameGameMode();

	virtual void BeginPlay() override;
	virtual void PostLogin(APlayerController* newPC) override;

	int maxNumOfPlayers;
	
	void SetupWeapons();
	UPROPERTY(EditDefaultsOnly, Category = Weapon)
		TArray<TSubclassOf<class AWeaponBase>> availableWeapons;

	UPROPERTY(EditDefaultsOnly, Category = Pickups)
		TArray<TSubclassOf<class APickup>> availablePickups;

	UFUNCTION() //UFUNCTION needed for timer!
		void spawnPickups();
	
	UFUNCTION() //UFUNCTION needed for timer!
		void UpdateTimer();
	
	FTimerHandle pickupSpawnTimer;
	FTimerHandle secondsTimer;
	FTimerHandle gameTimer;
	TArray<APickupSpawnVolume*> spawnVolumes;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float gameDuration;

	UPROPERTY(EditDefaultsOnly, Category = Pickups)
		float pickupSpawnDelay;
	float gameStartDelay;
	
	void gameOver();
	int GetRandomPickupIndex();
	void setupPickupSpawners();
};



