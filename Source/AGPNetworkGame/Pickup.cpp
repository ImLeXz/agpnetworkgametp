// Fill out your copyright notice in the Description page of Project Settings.


#include "Pickup.h"
#include "AGPNetworkGameCharacter.h"
#include "PickupSpawnVolume.h"

// Sets default values
APickup::APickup()
{
	PrimaryActorTick.bCanEverTick = false;
	collisionComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("collisionComponent"));
	staticMeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("staticMeshComponent"));

	RootComponent = collisionComponent;
	staticMeshComp->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void APickup::BeginPlay()
{
	Super::BeginPlay();
	collisionComponent->OnComponentBeginOverlap.AddDynamic(this, &APickup::OnBeginOverlap);
}

// Called every frame
void APickup::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APickup::MC_HandlePickup_Implementation(AAGPNetworkGameCharacter* chr)
{
	HandlePickup(chr);
}

void APickup::HandlePickup(AAGPNetworkGameCharacter* chr)
{
}

void APickup::MC_SetSpawnedAtVolume_Implementation(APickupSpawnVolume* volume)
{
	volumeSpawnedAt = volume;
}

void APickup::ServerSetSpawnedAtVolume_Implementation(APickupSpawnVolume* volume)
{
	MC_SetSpawnedAtVolume(volume);
}

void APickup::SetSpawnedAtVolume(APickupSpawnVolume* volume)
{
	if (GetLocalRole() == ROLE_Authority)
	{
		MC_SetSpawnedAtVolume(volume);
	}
	else
	{
		ServerSetSpawnedAtVolume(volume);
	}
}

void APickup::OnBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) {
	AAGPNetworkGameCharacter* chr = Cast<AAGPNetworkGameCharacter>(OtherActor);
	if (chr && chr->IsLocallyControlled()) {
		UE_LOG(LogTemp, Warning, TEXT("Player Picked Up: %s"), *name);
		
		if (chr->GetLocalRole() == ROLE_Authority)
		{
			MC_HandlePickup(chr);
		}
		else
		{
			chr->ServerHandlePickup(this);
		}
		volumeSpawnedAt->currentPickupsSpawned--;
	}

}
