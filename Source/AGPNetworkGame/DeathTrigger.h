// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/TriggerBox.h"
#include "DeathTrigger.generated.h"

/**
 * 
 */
UCLASS()
class AGPNETWORKGAME_API ADeathTrigger : public ATriggerBox
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable)
		void KillPlayer(AActor* actor);
};
