// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AGPNetworkGameCharacter.h"
#include "GameFramework/Actor.h"
#include "WeaponInterface.h"
#include "WeaponBase.generated.h"

UCLASS()
class AGPNETWORKGAME_API AWeaponBase : public AActor, public IWeaponInterface
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AWeaponBase();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	virtual void reloadWeapon() override;
	
	void reloadWeaponDelayed();
	virtual void SyncedWeaponAction(const FVector& startLoc, const FVector& endLoc, const FVector_NetQuantize& impactPoint, AActor* hitActor);

	//Stuff For Using Weapon
	UFUNCTION(NetMulticast, Reliable)
		void MC_UseWeapon(const FVector& startLoc, const FVector& endLoc, const FVector_NetQuantize& impactPoint, AActor* hitActor);
	void MC_UseWeapon_Implementation(const FVector& startLoc, const FVector& endLoc, const FVector_NetQuantize& impactPoint, AActor* hitActor);
	//--------------------------------------
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		AAGPNetworkGameCharacter* playerHeldBy;
	
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* staticMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool isGun;

	UPROPERTY(BlueprintReadWrite)
		bool isReloading;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float reloadTime;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int ammoPerMag;

	UPROPERTY(BlueprintReadWrite)
		int ammoInMag;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int ammoInWeapon;
};
