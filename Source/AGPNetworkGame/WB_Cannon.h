// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "WeaponBase.h"
#include "WB_Cannon.generated.h"

/**
 * 
 */
UCLASS()
class AGPNETWORKGAME_API AWB_Cannon : public AWeaponBase
{
	GENERATED_BODY()
	AWB_Cannon();

	virtual void SyncedWeaponAction(const FVector& startLoc, const FVector& endLoc, const FVector_NetQuantize& impactPoint, AActor* hitActor) override;
	virtual void BeginPlay() override;
	virtual void useWeapon(APawn* player) override; //MUST define virtual func!
	UPROPERTY(EditAnywhere, Category = Aim)
		FVector gunDir;
	UPROPERTY(EditAnywhere, Category = Aim)
		float gunDistance;
	UPROPERTY(EditAnywhere, Category = Damage)
		float damageAmt;
	UPROPERTY(EditAnywhere, Category = Damage)
		float damageRadius;
	UPROPERTY(EditAnywhere, Category = HitEffects)
		UParticleSystem* hitParticle;
	UPROPERTY(EditDefaultsOnly, Category = HitEffects)
		USoundBase* firingSound;
};
