// Fill out your copyright notice in the Description page of Project Settings.


#include "GoalTrigger.h"
#include "Components/BoxComponent.h"
#include "AGPNetworkGameCharacter.h"
#include "AGPNetworkGameGameMode.h"
#include "Kismet/GameplayStatics.h"


// Sets default values
AGoalTrigger::AGoalTrigger()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	collisionComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("collisionComponent"));
	RootComponent = collisionComponent;
	staticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("staticMeshComponent"));
	staticMeshComponent->SetupAttachment(RootComponent);
	staticMeshComponent->SetCollisionProfileName("OverlapAll");
	bReplicates = true;

}

// Called when the game starts or when spawned
void AGoalTrigger::BeginPlay()
{
	Super::BeginPlay();
	collisionComponent->OnComponentBeginOverlap.AddDynamic(this, &AGoalTrigger::OnBeginOverlap);
}

// Called every frame
void AGoalTrigger::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AGoalTrigger::OnBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) {
	AAGPNetworkGameCharacter* chr = Cast<AAGPNetworkGameCharacter>(OtherActor);
	if (chr && chr->IsLocallyControlled()) {
		UE_LOG(LogTemp, Warning, TEXT("OnBeginOverlap chr = %s"), *chr->GetName());
		if(chr->GetLocalRole() == ROLE_Authority && !isGoalForRemote)
		{
			chr->IncreasePlayerScore();
		}
		else if(chr->GetLocalRole() != ROLE_Authority && isGoalForRemote)
		{
			chr->IncreasePlayerScore();
		}
	}
}