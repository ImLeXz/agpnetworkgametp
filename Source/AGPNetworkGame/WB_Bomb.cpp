#include "WB_Bomb.h"
#include "DrawDebugHelpers.h"
#include "Engine/SkeletalMeshSocket.h"
#include "Kismet/GameplayStatics.h" //for effects
#include "AGPNetworkGameCharacter.h"
#include "Kismet/GameplayStatics.h" //for radial dmg

AWB_Bomb::AWB_Bomb() {
	onFloor = false;
	impulseVal = 800.0f;
	damageAmt = 110.0f;
	timeforBombToExplode = 1.5f;
	blastRadius = 300.0f;
	ammoInMag = 1;
	ammoPerMag = ammoInMag;
	ammoInWeapon = 1;
}

void AWB_Bomb::BeginPlay() {
	Super::BeginPlay();

	staticMesh->OnComponentHit.AddDynamic(this, &AWB_Bomb::OnHit);
	staticMesh->OnComponentBeginOverlap.AddDynamic(this, &AWB_Bomb::OnBeginOverlap);
}

void AWB_Bomb::useWeapon(APawn* player) {
	AAGPNetworkGameCharacter* chr = Cast<AAGPNetworkGameCharacter>(GetWorld()->GetFirstPlayerController()->GetPawn());
	if (chr) {
		chr->dropWeapon(this);
		staticMesh->SetSimulatePhysics(true);
		staticMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		staticMesh->AddImpulse(chr->GetActorForwardVector() * impulseVal, NAME_None, true);
		FTimerHandle animTimer;
		GetWorldTimerManager().SetTimer(animTimer, this, &AWB_Bomb::useWeaponAfterDelay, timeforBombToExplode, false);
	}
}

void AWB_Bomb::useWeaponAfterDelay() {
	TArray<AActor*> ignoreActors;
	UGameplayStatics::ApplyRadialDamage(GetWorld(), damageAmt, GetActorLocation(), blastRadius, UDamageType::StaticClass(), ignoreActors, this, nullptr, true);

	if (bombSound)
		UGameplayStatics::PlaySoundAtLocation(this, bombSound, GetActorLocation());
	if (hitParticle)
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), hitParticle, GetActorLocation(), FRotator::ZeroRotator, true);

	FTimerHandle destroyTimer;
	GetWorldTimerManager().SetTimer(destroyTimer, this, &AWB_Bomb::destroyWeapon, 0.3f, false);
}

void AWB_Bomb::destroyWeapon() {
	Destroy();
}

//Note: staticMesh should have Overlap set for Pawn (NOT Block)
void AWB_Bomb::OnBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) {
	AAGPNetworkGameCharacter* chr = Cast<AAGPNetworkGameCharacter>(OtherActor);
	if (onFloor && chr) {
		chr->grabWeapon(this);
		onFloor = false;
	}
}

// in static mesh set Simul generates hit event to true
void AWB_Bomb::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit) {
	if (OtherActor->GetName().Find("Floor") >= 0)
		onFloor = true;
}