// Fill out your copyright notice in the Description page of Project Settings.


#include "PickupSpawnVolume.h"

#include "Pickup.h"
#include "Components/BoxComponent.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
APickupSpawnVolume::APickupSpawnVolume()
{
	PrimaryActorTick.bCanEverTick = false;

	// Use a Box for the spawn volume.
	whereToSpawn = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxComponent"));
	whereToSpawn->SetBoxExtent(FVector(250.0, 250.0, 10.0));
	whereToSpawn->SetupAttachment(RootComponent);

	maxPickupsSpawnable = 5;
	currentPickupsSpawned = 0;
}

// Called when the game starts or when spawned
void APickupSpawnVolume::BeginPlay()
{
	Super::BeginPlay();
	currentPickupsSpawned = 0;
}

// Called every frame
void APickupSpawnVolume::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

FVector APickupSpawnVolume::getRandomPtInVolume()
{
	FVector spawnOrigin = whereToSpawn->Bounds.Origin;
	FVector spawnExtent = whereToSpawn->Bounds.BoxExtent;
	return UKismetMathLibrary::RandomPointInBoundingBox(spawnOrigin, spawnExtent);
}

void APickupSpawnVolume::spawnAnActor(TSubclassOf<AActor> actor)
{
	if (GetWorld() && currentPickupsSpawned < maxPickupsSpawnable)
	{
		//UE_LOG(LogTemp, Warning, TEXT("Spawning Pickup %s In Volume, Current Pickups Spawned: %i"), *actor->GetName(), currentPickupsSpawned);
		FActorSpawnParameters spawnParams;
		spawnParams.Owner = this;
		spawnParams.Instigator = GetInstigator();
		spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;

		FVector spawnLoc = getRandomPtInVolume();
		FRotator rot = FRotator::ZeroRotator;
		AActor* a = GetWorld()->SpawnActor<AActor>(actor, spawnLoc, rot, spawnParams);
		APickup* pickup = Cast<APickup>(a);
		pickup->SetSpawnedAtVolume(this);
		currentPickupsSpawned++;
	}
}
