// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ScoreHandler.generated.h"

UCLASS()
class AGPNETWORKGAME_API AScoreHandler : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AScoreHandler();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		TArray<class UWidget*> widgetComponentArray;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		FColor remotePlayerColor;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		FColor serverPlayerColor;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		int winningScore;
	
	void UpdateScore(bool isRemotePlayer);
	int currentScorePoint;
};
