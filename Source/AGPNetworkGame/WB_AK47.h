// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "WeaponBase.h"
#include "WB_AK47.generated.h"

/**
 *
 */
UCLASS()
class AGPNETWORKGAME_API AWB_AK47 : public AWeaponBase
{
	GENERATED_BODY()
public:
	AWB_AK47();

	virtual void SyncedWeaponAction(const FVector& startLoc, const FVector& endLoc, const FVector_NetQuantize& impactPoint, AActor* hitActor) override;
	virtual void BeginPlay() override;
	virtual void useWeapon(APawn* player) override; //MUST define virtual func!
	UPROPERTY(EditAnywhere, Category = Aim)
		FVector gunDir;
	UPROPERTY(EditAnywhere, Category = Aim)
		float gunDistance;
	UPROPERTY(EditAnywhere, Category = Damage)
		float damageAmt;
	UPROPERTY(EditAnywhere, Category = HitEffects)
		UParticleSystem* hitParticle;
	UPROPERTY(EditDefaultsOnly, Category = HitEffects)
		USoundBase* firingSound;
};
