// Fill out your copyright notice in the Description page of Project Settings.


#include "WB_Cannon.h"
#include "DrawDebugHelpers.h"
#include "Engine/SkeletalMeshSocket.h"
#include "Kismet/GameplayStatics.h" //for effects
#include "AGPNetworkGameCharacter.h"

AWB_Cannon::AWB_Cannon() {
	gunDistance = 50.0f;
	gunDir = FVector(0.0f, 0.0f, 0.176f);
	isGun = true;
	damageAmt = 40.0f;
	damageRadius = 10.0f;
	ammoInMag = 2;
	ammoPerMag = ammoInMag;
	ammoInWeapon = 10;
	reloadTime = 4.0f;
}

void AWB_Cannon::SyncedWeaponAction(const FVector& startLoc, const FVector& endLoc, const FVector_NetQuantize& impactPoint, AActor* hitActor)
{
	//UE_LOG(LogTemp, Warning, TEXT("Multicast Ran"));
	//UE_LOG(LogTemp, Warning, TEXT("Server Ray Trace From %s to %s"), *startLoc.ToString(), *endLoc.ToString());
	
	if (firingSound)
	{
		UGameplayStatics::PlaySoundAtLocation(this, firingSound, startLoc);
	}
	
	DrawDebugLine(GetWorld(), startLoc, endLoc, FColor::Yellow, true);
	DrawDebugSphere(GetWorld(), impactPoint, damageRadius, 16, FColor::Yellow, true);
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), hitParticle, impactPoint, FRotator::ZeroRotator, true);
	TArray<AActor*> ignoreActors;
	UGameplayStatics::ApplyRadialDamage(GetWorld(), damageAmt, impactPoint, damageRadius,
	UDamageType::StaticClass(), ignoreActors, this, nullptr, true); 
}

void AWB_Cannon::BeginPlay() {
}

void AWB_Cannon::useWeapon(APawn* player) {
	AAGPNetworkGameCharacter* chr = Cast<AAGPNetworkGameCharacter>(player);
	if (chr && chr->IsLocallyControlled())
	{
		if (!isReloading)
		{ 
			if (ammoInMag > 0)
			{
				FVector playerFacingDir = chr->GetFacingDirection();
				FVector dir = playerFacingDir + gunDir;
				dir.Normalize();
				FVector startLoc = chr->GetMesh()->GetSocketLocation(FName("WeaponSocket")) + dir * gunDistance;
				FVector endLoc = startLoc + chr->GetFacingDirection() * 5000.0f;
				FHitResult Hit;
				FCollisionQueryParams TraceParams(FName(TEXT("FiringTrace")), true, this);
				TraceParams.bTraceComplex = false;
				GetWorld()->LineTraceSingleByChannel(Hit, startLoc, endLoc, ECC_PhysicsBody, TraceParams);
				ammoInMag--;
				UE_LOG(LogTemp, Warning, TEXT("Shooting Gun!, Remaining Ammo: %i"), ammoInMag);
				
				chr->RotatePlayer();
				
				if (chr->GetLocalRole() == ROLE_Authority)
				{
					MC_UseWeapon(startLoc, endLoc, Hit.ImpactPoint, Hit.GetActor());
				}
				
				else
				{
					chr->ServerUseWeapon(this, startLoc, endLoc, Hit.ImpactPoint, Hit.GetActor());
				}
			}

			if (ammoInMag <= 0 && ammoInWeapon > 0)
			{
				UE_LOG(LogTemp, Warning, TEXT("Reloading Weapon!"));
				reloadWeapon();
			}
		}
	}
}