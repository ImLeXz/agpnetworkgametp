// Fill out your copyright notice in the Description page of Project Settings.


#include "ScoreHandler.h"

#include "Components/Image.h"
#include "Components/Widget.h"

// Sets default values
AScoreHandler::AScoreHandler()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	currentScorePoint = 0;
	winningScore = 3;
}

// Called when the game starts or when spawned
void AScoreHandler::BeginPlay()
{
	Super::BeginPlay();
	
}

void AScoreHandler::UpdateScore(bool isRemotePlayer)
{
	UWidget* scoreWidget = widgetComponentArray[currentScorePoint];
	UImage* scoreImageWidget = Cast<UImage>(scoreWidget);
	if(isRemotePlayer) scoreImageWidget->SetColorAndOpacity(remotePlayerColor);
	else scoreImageWidget->SetColorAndOpacity(serverPlayerColor);
	currentScorePoint++;
}

