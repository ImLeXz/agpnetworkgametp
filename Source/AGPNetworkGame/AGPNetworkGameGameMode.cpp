// Copyright Epic Games, Inc. All Rights Reserved.

#include "AGPNetworkGameGameMode.h"
#include "AGPNetworkGameCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "GameFramework/GameState.h"
#include "GameFramework/PlayerState.h"
#include "EngineUtils.h" //for ActorIterator
#include "Components/Widget.h"
#include "AGPNetworkGameState.h"
#include "Components/Image.h"
#include "WeaponBase.h"

AAGPNetworkGameGameMode::AAGPNetworkGameGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL) {
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
	gameDuration = 300.0f;
	maxNumOfPlayers = 2;
	gameStartDelay = 1.5f;
}

void AAGPNetworkGameGameMode::BeginPlay() {
	Super::BeginPlay();
}

void AAGPNetworkGameGameMode::PostLogin(APlayerController* newPC) {
	Super::PostLogin(newPC);
	AAGPNetworkGameState* gs = GetGameState<AAGPNetworkGameState>();
	if (gs && gs->PlayerArray.Num() == maxNumOfPlayers) {
		gs->gameTime = gameDuration;
		UE_LOG(LogTemp, Warning, TEXT("-----------[GAME STARTING]-----------"));
		UE_LOG(LogTemp, Warning, TEXT("PostLogin Num players: %d"), gs->PlayerArray.Num());
		setupPickupSpawners();
		SetupWeapons();
		GetWorldTimerManager().SetTimer(gameTimer, this, &AAGPNetworkGameGameMode::gameOver, gameDuration, false);
		GetWorldTimerManager().SetTimer(secondsTimer, this, &AAGPNetworkGameGameMode::UpdateTimer, 1.0f, true);
		GetWorldTimerManager().SetTimer(pickupSpawnTimer, this, &AAGPNetworkGameGameMode::spawnPickups, pickupSpawnDelay, true);
	}
}

void AAGPNetworkGameGameMode::UpdateTimer() {
	AAGPNetworkGameState* gs = GetGameState<AAGPNetworkGameState>();
	gs->gameTime -= 1.0f;	//incr time
}

int AAGPNetworkGameGameMode::GetRandomPickupIndex()
{
	int randomIndex = FMath::RandRange(0, availablePickups.Num() - 1);
	return randomIndex;
}

void AAGPNetworkGameGameMode::setupPickupSpawners()
{
	UE_LOG(LogTemp, Warning, TEXT("-----------[PICKUPS SPAWNED]-----------"));

	for (TActorIterator<APickupSpawnVolume> ActorItr(GetWorld()); ActorItr; ++ActorItr) {
		APickupSpawnVolume* actor = *ActorItr;
		spawnVolumes.Add(actor);
	}
}

void AAGPNetworkGameGameMode::spawnPickups()
{
	for(size_t i = 0; i < spawnVolumes.Num(); i++)
	{
		spawnVolumes[i]->spawnAnActor(availablePickups[GetRandomPickupIndex()]);
	}
}

void AAGPNetworkGameGameMode::gameOver() {
	UE_LOG(LogTemp, Warning, TEXT("GAMEMODE GAMEOVER"));
	
	AAGPNetworkGameState* gs = GetGameState<AAGPNetworkGameState>();  //get server character
	AAGPNetworkGameCharacter* chr = Cast<AAGPNetworkGameCharacter>(gs->PlayerArray[0]->GetPawn());
	GetWorldTimerManager().ClearTimer(gameTimer);
	GetWorldTimerManager().ClearTimer(secondsTimer);
	GetWorldTimerManager().ClearTimer(pickupSpawnTimer);
	chr->MC_Endgame(); //Calls end game on server and multi casts it to connected players
}

void AAGPNetworkGameGameMode::SetupWeapons()
{
	AAGPNetworkGameState* gs = GetGameState<AAGPNetworkGameState>();
	for(size_t i = 0; i < gs->PlayerArray.Num(); i++)
	{
		AAGPNetworkGameCharacter* chr = Cast<AAGPNetworkGameCharacter>(gs->PlayerArray[i]->GetPawn());
		for (size_t j = 0; j < availableWeapons.Num(); j++)
		{
			FActorSpawnParameters spawnParams;
			spawnParams.Owner = this;
			spawnParams.Instigator = GetInstigator();
			spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;

			//FVector loc = FVector(-390, 0, 380);
			FVector loc = FVector(0, 0, 0);
			FRotator rot = FRotator::ZeroRotator;
			AActor* weaponActor = GetWorld()->SpawnActor<AActor>(availableWeapons[j], loc, rot, spawnParams);
			AWeaponBase* weaponBase = Cast<AWeaponBase>(weaponActor);
			weaponBase->playerHeldBy = chr;
			
			FString actorName = weaponBase->GetName();
			UE_LOG(LogTemp, Warning, TEXT("Added: %s"), *actorName);
		}
	}

	FTimerHandle gameStartDelayTimer;
	AAGPNetworkGameCharacter* server = Cast<AAGPNetworkGameCharacter>(gs->PlayerArray[0]->GetPawn());
	GetWorldTimerManager().SetTimer(gameStartDelayTimer, server, &AAGPNetworkGameCharacter::AssignWeapons, gameStartDelay, false);
}
