// Fill out your copyright notice in the Description page of Project Settings.


#include "Trigger.h"

#include "AGPNetworkGameCharacter.h"

// Sets default values
ATrigger::ATrigger()
{
	PrimaryActorTick.bCanEverTick = false;
	collisionComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("collisionComponent"));

	RootComponent = collisionComponent;
	collisionComponent->SetSimulatePhysics(false);
	collisionComponent->SetCollisionProfileName(TEXT("PhysicsActor"));
	collisionComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);


}

// Called when the game starts or when spawned
void ATrigger::BeginPlay()
{
	Super::BeginPlay();
	
}

void ATrigger::TriggerFunction(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp)
{
}

// Called every frame
void ATrigger::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATrigger::OnEndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) {
	AAGPNetworkGameCharacter* chr = Cast<AAGPNetworkGameCharacter>(OtherActor);
	if (chr) {
		UE_LOG(LogTemp, Warning, TEXT("Hit chr = %s"), *chr->GetName());
		TriggerFunction(OverlappedComp, OtherActor, OtherComp);
	}
}