// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "ScoreHandler.h"
#include "GameFramework/Character.h"
#include "AGPNetworkGameCharacter.generated.h"

UCLASS(config=Game)
class AAGPNetworkGameCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;
	
public:
	AAGPNetworkGameCharacter();

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;

protected:
	virtual void BeginPlay();
	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser);
protected:

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/** 
	 * Called via input to turn at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

	UPROPERTY(BlueprintReadWrite, Category = Stats)
		float currentSpeed;

	UPROPERTY(BlueprintReadWrite, Category = Stats)
		float initialSpeed;

	UPROPERTY(BlueprintReadWrite, Category = Weapon)
		int selectedWeaponNum;

	UPROPERTY(BlueprintReadWrite, Category = Weapon)
		bool hasGun;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon)
		FString weaponSocketName;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon)
		TArray<class AWeaponBase*> equipedWeapons;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon)
		class AWeaponBase* currentWeapon;

	FVector GetFacingDirection();
	FRotator GetCameraRotation();
	FVector initialLocation;
	FRotator initialRotation;

	UFUNCTION(NetMulticast, Reliable)
		void MC_AssignWeapon(class AWeaponBase* wb, AAGPNetworkGameCharacter* chr);
	void MC_AssignWeapon_Implementation(class AWeaponBase* wb, AAGPNetworkGameCharacter* chr);
	void AssignWeapons();
	
	void grabWeapon(class AWeaponBase* wb);
	int GetPlayerID();

	//Stuff For Dropping Weapon
	UFUNCTION(NetMulticast, Reliable)
		void MC_UpdateWeaponPos(class AWeaponBase* wb, const FVector& newLoc);
	void MC_UpdateWeaponPos_Implementation(class AWeaponBase* wb, const FVector& newLoc);

	UFUNCTION(NetMulticast, Reliable)
		void MC_DropWeapon(class AWeaponBase* wb, const FVector& newLoc);
	void MC_DropWeapon_Implementation(class AWeaponBase* wb, const FVector& newLoc);

	UFUNCTION(Server, Reliable)
		void ServerDropWeapon(class AWeaponBase* wb, const FVector& newLoc);
	void ServerDropWeapon_Implementation(class AWeaponBase* wb, const FVector& newLoc);

	void dropWeapon(class AWeaponBase* wb);	//called when key pressed
	//--------------------------------------

	//Stuff For Switching Weapon
	UFUNCTION(NetMulticast, Reliable)
		void MC_SwitchWeapon(class AWeaponBase* wb, class AWeaponBase* previousWb);
	void MC_SwitchWeapon_Implementation(class AWeaponBase* wb, class AWeaponBase* previousWb);
	UFUNCTION(Server, Reliable)
		void ServerSwitchWeapon(class AWeaponBase* wb, class AWeaponBase* previousWb);
	void ServerSwitchWeapon_Implementation(class AWeaponBase* wb, class AWeaponBase* previousWb);
	void SwitchWeapon(class AWeaponBase* wb, class AWeaponBase* previousWb);

	UFUNCTION(BlueprintCallable)
		void SwitchWeaponNext();
	UFUNCTION(BlueprintCallable)
		void SwitchWeaponPrevious();
	//--------------------------------------

	void reloadWeapon();

	//Stuff For Using Weapon
	
	UFUNCTION(Server, Reliable)
		void ServerUseWeapon(class AWeaponBase* wb, const FVector& startLoc, const FVector& endLoc, const FVector_NetQuantize& impactPoint, AActor* hitActor);
	void ServerUseWeapon_Implementation(class AWeaponBase* wb, const FVector& startLoc, const FVector& endLoc, const FVector_NetQuantize& impactPoint, AActor* hitActor);
	
	void useWeapon();
	//--------------------------------------

	UFUNCTION(NetMulticast, Reliable)
		void MC_UpdateRotation(const FRotator& newRot);
	void MC_UpdateRotation_Implementation(const FRotator& newRot);
	UFUNCTION(Server, Reliable)
		void ServerUpdateRotation(const FRotator& newRot);
	void ServerUpdateRotation_Implementation(const FRotator& newRot);
	void RotatePlayer();

	//Stuff For Health
	UFUNCTION(NetMulticast, Reliable)
		void MC_DeathAction();
	void MC_DeathAction_Implementation();

	UFUNCTION(Server, Reliable)
		void ServerDeathAction();
	void ServerDeathAction_Implementation();
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Stats)
		bool isInvulnerable;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Stats)
		float currentHealth;
	float initialHealth;
	
	void updateHealth(float newHealth);
	//--------------------------------------

	//Pickups
	UFUNCTION(Server, Reliable)
		void ServerHandlePickup(class APickup* pickup);
	void ServerHandlePickup_Implementation(class APickup* pickup);

	UFUNCTION()
		void ResetSpeed();
	UFUNCTION()
		void IncreaseSpeed(float increase);

	UFUNCTION(NetMulticast, Reliable)
		void MC_UpdateScore(bool isRemote);
	void MC_UpdateScore_Implementation(bool isRemote);
	
	UFUNCTION(Server, Reliable)
		void ServerUpdateScore(bool isRemote);
	void ServerUpdateScore_Implementation(bool isRemote);

	void IncreasePlayerScore();
	
	UPROPERTY(BlueprintReadWrite, Category = Score)
		AScoreHandler* scoreHandler;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Score)
		int currentScore;

	UFUNCTION(NetMulticast, Reliable)
		void MC_SetWinningPlayer(bool isRemote);
	void MC_SetWinningPlayer_Implementation(bool isRemote);
	UFUNCTION(Server, Reliable)
		void ServerSetWinningPlayer(bool isRemote);
	void ServerSetWinningPlayer_Implementation(bool isRemote);

	UFUNCTION(BlueprintImplementableEvent)
		void GameOverBPEvent(bool hasWon);

	UFUNCTION(BlueprintImplementableEvent)
		void InitialisePlayer();
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool isInitialised;

	UFUNCTION(NetMulticast, Reliable)
		void MC_Endgame();
	void MC_Endgame_Implementation();
};

