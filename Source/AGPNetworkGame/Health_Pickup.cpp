// Fill out your copyright notice in the Description page of Project Settings.


#include "Health_Pickup.h"
#include "AGPNetworkGameCharacter.h"

void AHealth_Pickup::HandlePickup(AAGPNetworkGameCharacter* Chr)
{
    if(Chr != nullptr)
    {
		Chr->updateHealth(Chr->currentHealth + healthIncrease);
		Destroy();
	}
}
