// Copyright Epic Games, Inc. All Rights Reserved.

#include "AGPNetworkGameCharacter.h"

#include "AGPNetworkGameGameMode.h"
#include "EngineUtils.h"
#include "Pickup.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "WeaponBase.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/PlayerState.h"

//////////////////////////////////////////////////////////////////////////
// AAGPNetworkGameCharacter

AAGPNetworkGameCharacter::AAGPNetworkGameCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)

	weaponSocketName = "WeaponSocket";
	currentScore = 0;
	isInitialised = false;
	updateHealth(100.0f);
}

//////////////////////////////////////////////////////////////////////////
// Input

void AAGPNetworkGameCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAxis("MoveForward", this, &AAGPNetworkGameCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AAGPNetworkGameCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AAGPNetworkGameCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AAGPNetworkGameCharacter::LookUpAtRate);

	PlayerInputComponent->BindAction("WeaponNext", IE_Pressed, this, &AAGPNetworkGameCharacter::SwitchWeaponNext);
	PlayerInputComponent->BindAction("WeaponPrevious", IE_Pressed, this, &AAGPNetworkGameCharacter::SwitchWeaponPrevious);
	PlayerInputComponent->BindAction("UseWeapon", IE_Pressed, this, &AAGPNetworkGameCharacter::useWeapon);
	PlayerInputComponent->BindAction("ReloadWeapon", IE_Pressed, this, &AAGPNetworkGameCharacter::reloadWeapon);
}

void AAGPNetworkGameCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();
	isInvulnerable = true;
	initialLocation = this->GetActorLocation();
	initialRotation = this->GetActorRotation();
	initialHealth = currentHealth;
	initialSpeed = this->GetCharacterMovement()->MaxWalkSpeed;
	currentSpeed = initialSpeed;

	//Delay switching to default weapon to allow time for both players to load
	//FTimerHandle delayedWeaponSwitch;
	//GetWorldTimerManager().SetTimer(delayedWeaponSwitch, this, &AAGPNetworkGameCharacter::SwitchWeaponNext, startupWeaponSwitchDelay, false);
}

template<typename T>
void FindAllActors(UWorld* World, TArray<T*>& Out)
{
	for (TActorIterator<T> It(World); It; ++It)
	{
		Out.Add(*It);
	}
}

void AAGPNetworkGameCharacter::MC_AssignWeapon_Implementation(AWeaponBase* wb, AAGPNetworkGameCharacter* chr)
{
	if(wb != nullptr && chr != nullptr)
	{
		chr->equipedWeapons.Add(wb);
		wb->staticMesh->SetSimulatePhysics(false);
		wb->staticMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		
		if(chr->isInitialised == false)
		{
			chr->InitialisePlayer();
		}
	}
}


void AAGPNetworkGameCharacter::AssignWeapons()
{
	if(IsLocallyControlled())
	{
		TArray<AWeaponBase*> allWeapons;
		FindAllActors(GetWorld(), allWeapons);
		AAGPNetworkGameCharacter* chr = nullptr;
		AWeaponBase* wb = nullptr;
		for(size_t i = 0; i < allWeapons.Num(); i++)
		{
			chr = allWeapons[i]->playerHeldBy;
			wb = allWeapons[i];
			if(chr != nullptr && wb != nullptr)
			{
				MC_AssignWeapon(wb, chr);
			}
		}
	}
}


int AAGPNetworkGameCharacter::GetPlayerID() {
	APlayerState* ps = GetPlayerState();  //get default player state
	return ps->GetPlayerId(); //get unique player ID
}

void AAGPNetworkGameCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AAGPNetworkGameCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AAGPNetworkGameCharacter::MoveForward(float Value)
{
	if ((Controller != nullptr) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
		
	}
}

void AAGPNetworkGameCharacter::MoveRight(float Value)
{
	if ( (Controller != nullptr) && (Value != 0.0f) )
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
	
		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}


FVector AAGPNetworkGameCharacter::GetFacingDirection()
{
	return FollowCamera->GetComponentRotation().Vector();
}

FRotator AAGPNetworkGameCharacter::GetCameraRotation()
{
	return FollowCamera->GetComponentRotation();
}


void AAGPNetworkGameCharacter::grabWeapon(AWeaponBase* wb)
{
	if(IsLocallyControlled())
	{
		if (wb) {
			equipedWeapons.Add(wb);
			SwitchWeapon(wb, nullptr);
		}
	}
}

void AAGPNetworkGameCharacter::reloadWeapon() {
	if (IsLocallyControlled())
	{
		currentWeapon->reloadWeapon();
	}
}

void AAGPNetworkGameCharacter::ServerUseWeapon_Implementation(AWeaponBase* wb, const FVector& startLoc, const FVector& endLoc, const FVector_NetQuantize& impactPoint, AActor* hitActor)
{
	//UE_LOG(LogTemp, Warning, TEXT("Server Func Ran"));
	wb->MC_UseWeapon(startLoc, endLoc, impactPoint, hitActor);
}

void AAGPNetworkGameCharacter::useWeapon() {
	if(IsLocallyControlled())
	{
		if (currentWeapon && !isInvulnerable)
		{
			currentWeapon->useWeapon(this);
		}
	}
}

void AAGPNetworkGameCharacter::MC_UpdateRotation_Implementation(const FRotator& newRot)
{
	SetActorRotation(newRot);
}

void AAGPNetworkGameCharacter::ServerUpdateRotation_Implementation(const FRotator& newRot)
{
	MC_UpdateRotation(newRot);
}

void AAGPNetworkGameCharacter::RotatePlayer()
{
	if(IsLocallyControlled())
	{
		FRotator charRotation = GetActorRotation();
		FRotator targetRotation = GetCameraRotation();
		FRotator newRot = FRotator(charRotation.Pitch, targetRotation.Yaw, charRotation.Roll);
		SetActorRotation(newRot); //Set locally first to ensure rotated correct way
		UE_LOG(LogTemp, Warning, TEXT("Rotating Player To: %s"), *newRot.ToString());

		if (GetLocalRole() == ROLE_Authority)
		{
			MC_UpdateRotation(newRot); //Update rotation (from server)
		}
				
		else
		{
			ServerUpdateRotation(newRot); //Update rotation (from remote)
		}
	}
}


void AAGPNetworkGameCharacter::SwitchWeaponPrevious()
{
	if(IsLocallyControlled())
	{
		if (equipedWeapons.Last() != nullptr)
		{
			int prevWep = selectedWeaponNum;
			selectedWeaponNum--;

			if (selectedWeaponNum < 0)
				selectedWeaponNum = equipedWeapons.Num() - 1;

			UE_LOG(LogTemp, Warning, TEXT("Player %s, Switched Weapons To: %i"), *GetName(), selectedWeaponNum);
			SwitchWeapon(equipedWeapons[selectedWeaponNum], equipedWeapons[prevWep]);
		}
	}
}

void AAGPNetworkGameCharacter::SwitchWeaponNext()
{
	if (IsLocallyControlled())
	{
		if (equipedWeapons.Last() != nullptr)
		{
			int prevWep = selectedWeaponNum;
			selectedWeaponNum++;

			if (selectedWeaponNum > equipedWeapons.Num() - 1)
				selectedWeaponNum = 0;

			UE_LOG(LogTemp, Warning, TEXT("Player %s, Switched Weapons To: %i"), *GetName(), selectedWeaponNum);
			SwitchWeapon(equipedWeapons[selectedWeaponNum], equipedWeapons[prevWep]);
		}
	}
}


void AAGPNetworkGameCharacter::SwitchWeapon(AWeaponBase* wb, AWeaponBase* previousWeapon)
{
	if(IsLocallyControlled())
	{
		if(wb)
		{
			USkeletalMeshSocket const* Socket = GetMesh()->GetSocketByName(FName(weaponSocketName));
			if (Socket) {
				currentWeapon = wb;
				if (GetLocalRole() == ROLE_Authority) {
					MC_SwitchWeapon(wb, previousWeapon);
				}
				else
					ServerSwitchWeapon(wb, previousWeapon);
			}
		}
	}
}

void AAGPNetworkGameCharacter::MC_SwitchWeapon_Implementation(AWeaponBase* wb, AWeaponBase* previousWb)
{
	if (wb)
	{
		hasGun = wb->isGun;
		wb->staticMesh->SetVisibility(true);
		wb->AttachToComponent(GetMesh(), FAttachmentTransformRules(EAttachmentRule::SnapToTarget,
	EAttachmentRule::SnapToTarget, EAttachmentRule::KeepWorld, true), FName(weaponSocketName));
		if(previousWb != nullptr)
		{
			previousWb->staticMesh->SetVisibility(false);
		}
		wb->staticMesh->Activate();
	}
}

void AAGPNetworkGameCharacter::ServerSwitchWeapon_Implementation(AWeaponBase* wb, AWeaponBase* previousWb)
{
	MC_SwitchWeapon(wb, previousWb);
}

void AAGPNetworkGameCharacter::MC_DeathAction_Implementation()
{
	this->SetActorLocationAndRotation(initialLocation, initialRotation);
}

void AAGPNetworkGameCharacter::ServerDeathAction_Implementation()
{
	MC_DeathAction();
}

void AAGPNetworkGameCharacter::updateHealth(float newHealth) {
	UE_LOG(LogTemp, Warning, TEXT("Updated Health To: %f"), newHealth);
	currentHealth = newHealth;
	if(currentHealth <= 0)
	{
		UE_LOG(LogTemp, Warning, TEXT("Killed!"));
		currentHealth = initialHealth;
		if (GetLocalRole() == ROLE_Authority) {
			MC_DeathAction();
		}
		else
			ServerDeathAction();
		isInvulnerable = true;
	}
}

void AAGPNetworkGameCharacter::ServerHandlePickup_Implementation(APickup* pickup)
{
	pickup->MC_HandlePickup(this);
}

void AAGPNetworkGameCharacter::ResetSpeed()
{
	if(this != nullptr)
	{
		currentSpeed = initialSpeed;
		this->GetCharacterMovement()->MaxWalkSpeed = currentSpeed;
		
		/*
		if(IsLocallyControlled())
		{
			currentSpeed = initialSpeed;
			if (GetLocalRole() == ROLE_Authority)
			{
				MC_UpdateSpeed(currentSpeed);
			}
			else
			{
				ServerUpdateSpeed(currentSpeed);
			} 
		}
		*/
	}
}

void AAGPNetworkGameCharacter::IncreaseSpeed(float increase)
{
	if(this != nullptr)
	{
		currentSpeed *= increase;
		this->GetCharacterMovement()->MaxWalkSpeed = currentSpeed;
		
		/*
		if(IsLocallyControlled())
		{
			currentSpeed *= increase;
			if (GetLocalRole() == ROLE_Authority)
			{
				MC_UpdateSpeed(currentSpeed);
			}
			else
			{
				ServerUpdateSpeed(currentSpeed);
			}
		}
		*/
	}
}


float AAGPNetworkGameCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) {
	if(!isInvulnerable && IsLocallyControlled())
	{
		float newHealth = currentHealth - DamageAmount;
		UE_LOG(LogTemp, Warning, TEXT("Dealing %f damage to %s"), DamageAmount, *this->GetName());
		UE_LOG(LogTemp, Warning, TEXT("New health should be %f"), newHealth);
		updateHealth(newHealth);
	}
	return currentHealth;
}


void AAGPNetworkGameCharacter::MC_UpdateWeaponPos_Implementation(AWeaponBase* wb, const FVector& newLoc)
{
	if (wb)
		wb->SetActorLocation(newLoc); //replic movement must be on!
}

void AAGPNetworkGameCharacter::MC_DropWeapon_Implementation(AWeaponBase* wb, const FVector& newLoc) {
	wb->DetachFromActor(FDetachmentTransformRules(EDetachmentRule::KeepWorld, EDetachmentRule::KeepWorld, EDetachmentRule::KeepWorld, true));
	MC_UpdateWeaponPos(wb, newLoc);
}

void AAGPNetworkGameCharacter::ServerDropWeapon_Implementation(AWeaponBase* wb, const FVector& newLoc) {
	MC_DropWeapon(wb, newLoc);
}

void AAGPNetworkGameCharacter::dropWeapon(AWeaponBase* wb) {
	if (wb && IsLocallyControlled()) {
		AWeaponBase* weapon = wb;  //store before dropping!
		FVector grabPos = wb->GetActorLocation();
		equipedWeapons.Remove(wb);
		currentWeapon = nullptr;
		selectedWeaponNum = 0;
		//SwitchWeapon(equipedWeapons[selectedWeaponNum], nullptr);

		if (GetLocalRole() == ROLE_Authority) {
			MC_DropWeapon(weapon, grabPos);
		}
		else
			ServerDropWeapon(weapon, grabPos);
	}
}

void AAGPNetworkGameCharacter::MC_Endgame_Implementation()
{
	GameOverBPEvent(false);
}


void AAGPNetworkGameCharacter::MC_SetWinningPlayer_Implementation(bool isRemote)
{
	if(GetLocalRole() == ROLE_Authority)
	{
		GameOverBPEvent(!isRemote);
	}
	else
	{
		GameOverBPEvent(isRemote);
	}
}

void AAGPNetworkGameCharacter::ServerSetWinningPlayer_Implementation(bool isRemote)
{
	MC_SetWinningPlayer(isRemote);
}

void AAGPNetworkGameCharacter::MC_UpdateScore_Implementation(bool isRemote)
{
	if(scoreHandler != nullptr)
	{
		scoreHandler->UpdateScore(isRemote);
	}
}

void AAGPNetworkGameCharacter::ServerUpdateScore_Implementation(bool isRemote)
{
	MC_UpdateScore(isRemote);
}


void AAGPNetworkGameCharacter::IncreasePlayerScore()
{
	if(this != nullptr && IsLocallyControlled())
	{
		bool hasWon = false;
		currentScore++;
		if(currentScore >= scoreHandler->winningScore) hasWon = true;
		
		if(GetLocalRole() == ROLE_Authority)
		{
			if(hasWon) MC_SetWinningPlayer(false);
			MC_DeathAction();
			MC_UpdateScore(false);
		}
			
		else
		{
			if(hasWon) ServerSetWinningPlayer(true);
			ServerDeathAction();
			ServerUpdateScore(true);
		}
		UE_LOG(LogTemp, Warning, TEXT("Increased %s Score To: %i"), *this->GetName(), currentScore);
	}
}
