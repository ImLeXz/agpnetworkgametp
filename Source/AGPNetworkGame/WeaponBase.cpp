// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponBase.h"
#include "AGPNetworkGameCharacter.h"

// Sets default values
AWeaponBase::AWeaponBase()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	staticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("staticMesh"));
	staticMesh->SetSimulatePhysics(true);
	RootComponent = staticMesh;
	isGun = false;
	isReloading = false;
	ammoInMag = 30;
	ammoPerMag = ammoInMag;
	ammoInWeapon = 120;
	reloadTime = 1.5f;
}

// Called when the game starts or when spawned
void AWeaponBase::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AWeaponBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AWeaponBase::reloadWeapon() {
	isReloading = true;
	FTimerHandle animTimer;
	GetWorldTimerManager().SetTimer(animTimer, this, &AWeaponBase::reloadWeaponDelayed, reloadTime, false);
}

void AWeaponBase::reloadWeaponDelayed() {
	isReloading = false;
	UE_LOG(LogTemp, Warning, TEXT("Weapon Reloaded!"));
	ammoInWeapon = ammoInWeapon - (ammoPerMag - ammoInMag);
	UE_LOG(LogTemp, Warning, TEXT("New Ammo In Weapon: %i"), ammoInWeapon);
	int ammoReduction = 0;
	if (ammoInWeapon <= 0)
		ammoReduction = ammoInWeapon;
	ammoInMag = ammoPerMag + ammoReduction;
	UE_LOG(LogTemp, Warning, TEXT("New Ammo In Mag: %i"), ammoInMag);
}

void AWeaponBase::SyncedWeaponAction(const FVector& startLoc, const FVector& endLoc, const FVector_NetQuantize& impactPoint, AActor* hitActor)
{
	
}

void AWeaponBase::MC_UseWeapon_Implementation(const FVector& startLoc, const FVector& endLoc, const FVector_NetQuantize& impactPoint, AActor* hitActor)
{
	SyncedWeaponAction(startLoc, endLoc, impactPoint, hitActor);
}
