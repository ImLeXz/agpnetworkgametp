// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Pickup.h"
#include "SpeedBoost_Pickup.generated.h"

/**
 * 
 */
UCLASS()
class AGPNETWORKGAME_API ASpeedBoost_Pickup : public APickup
{
	GENERATED_BODY()
protected:
	virtual void HandlePickup(class AAGPNetworkGameCharacter* chr) override;
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = SpeedBoostPickup)
		float speedMultiplier;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = SpeedBoostPickup)
		float speedResetDelay;
};
