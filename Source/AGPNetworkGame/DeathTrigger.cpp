// Fill out your copyright notice in the Description page of Project Settings.


#include "DeathTrigger.h"

#include "AGPNetworkGameCharacter.h"

void ADeathTrigger::KillPlayer(AActor* actor)
{
	AAGPNetworkGameCharacter* chr = Cast<AAGPNetworkGameCharacter>(actor);
	if (chr)
	{
		chr->updateHealth(0);
	}
}
